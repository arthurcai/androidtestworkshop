package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;



public class DriverFactory {
    public static WebDriver driver = null;
    public static WebDriverWait waitVar = null;
    private static Logger Log = LogManager.getLogger(DriverFactory.class.getName());

    public void createDriver() throws MalformedURLException, InterruptedException {
        // set up appium
        final File classpathRoot = new File(System.getProperty("user.dir"));

        final File appDir = new File(classpathRoot, "src/test/resources/targetApps");
        final File app = new File(appDir, "eTtoday_phone-beta-debug.apk");
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "ehssensengo");
        capabilities.setCapability("platformVersion", "5.0.2");

//        capabilities.setCapability("deviceName", "FA327PN08762");
//        capabilities.setCapability("platformVersion", "4.4.2");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("fullReset", true);
        capabilities.setCapability("noReset", false);
        capabilities.setCapability("appPackage", "net.ettoday.phone");
        capabilities.setCapability("appActivity","net.ettoday.phone.Main");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        // initializing waits
        waitVar = new WebDriverWait(driver, 7);
        //Thread.sleep(90000);

    }

    public void teardown() {
        //close the app
        driver.quit();
    }
}
