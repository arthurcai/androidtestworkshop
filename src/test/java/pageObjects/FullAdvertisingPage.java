package pageObjects;

import common.CommonLib;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.DriverFactory;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class FullAdvertisingPage extends DriverFactory {

    private static Logger Log = LogManager.getLogger(FullAdvertisingPage.class.getName());

    public FullAdvertisingPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "et_image_view_container")
    private WebElement etImageViewContainer;

    @AndroidFindBy(id = "et_ad_cover_header")
    private WebElement etAdCoverHeader;

    public boolean isShown() throws Exception{
        waitVar.until(ExpectedConditions.visibilityOf(etAdCoverHeader));
        return CommonLib.isWebElementNotPresent(etAdCoverHeader,"etAdCoverHeader") &&
                CommonLib.isWebElementNotPresent(etImageViewContainer,"etImageViewContainer");
    }

    public void clickEtAdCoverHeader(){
        etAdCoverHeader.click();
    }

}
