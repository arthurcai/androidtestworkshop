package steps;

import java.net.MalformedURLException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import utils.DriverFactory;

public class Hooks {

    DriverFactory appium = new DriverFactory();

    @Before
    public void beforeHookfunction() throws MalformedURLException, InterruptedException {
        appium.createDriver();
    }

    @After
    public void afterHookfunction() {
        appium.teardown();
    }

}
