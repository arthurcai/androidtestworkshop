package common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import static junit.framework.TestCase.fail;

public class CommonLib {
    private static Logger Log = LogManager.getLogger(CommonLib.class.getName());
    public static void assertWebElementNotPresent(WebElement webElement, String webElementName) {
        try {
            Log.info("Checking if WebElement " + webElementName + " is present......");
            webElement.isDisplayed();
            Log.info("[Error] WebElement " + webElementName + " is present!");
            fail("[Error] WebElement " + webElementName + " is present!");
        } catch (NoSuchElementException ex) {
            Log.info("[OK] WebElement " + webElementName + " is not present.");
        /* do nothing, element is not present, assert is passed */
        }
    }

    public static boolean isWebElementNotPresent(WebElement webElement, String webElementName) {
        try {
            Log.info("Checking if WebElement " + webElementName + " is present......");
            boolean isDisplayed= webElement.isDisplayed();
            Log.info("[Result] WebElement " + webElementName + " is present!");
            return isDisplayed;
        } catch (NoSuchElementException ex) {
            Log.info("[Result] WebElement " + webElementName + " is not present.");
            return false;
        /* do nothing, element is not present, assert is passed */
        }
    }
}
