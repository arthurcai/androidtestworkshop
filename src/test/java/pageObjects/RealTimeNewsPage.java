package pageObjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.Content;
import utils.DriverFactory;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RealTimeNewsPage extends DriverFactory {

    ToolBar toolBar = new ToolBar();

    ActionBar actionBar = new ActionBar();

    @AndroidFindBy(id = "title")
    public List<WebElement> newsList;

    @AndroidFindBy(id = "tab_settings_button")
    public WebElement channelSubscription;

    @AndroidFindBy(id = "et_tab_layout")
    private WebElement etTabLayout;

    private static Logger Log = LogManager.getLogger(RealTimeNewsPage.class.getName());

    public RealTimeNewsPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void verifyShown() {
        waitVar.until(ExpectedConditions.visibilityOf(channelSubscription));
        toolBar.verifyShown();
        actionBar.verifyShown(Content.titleName.IMMEDIATENEWS);
        assertNotNull(newsList);
        assertTrue(newsList.get(0).isDisplayed());
        assertTrue(channelSubscription.isDisplayed());
    }

    public void clickNews(Integer index){
        if(!index.equals(null)){
            newsList.get(index).click();
        }
    }

    public void disappearChannel(String name){
        boolean isAppear = true;
        try{
            etTabLayout.findElement(By.name(name));
        }catch (NoSuchElementException e){
            isAppear = false;
        }finally {
            assertFalse(isAppear);
        }
    }

}
