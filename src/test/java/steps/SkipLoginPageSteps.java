package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.AskLoginPage;
import pageObjects.FullAdvertisingPage;
import pageObjects.RealTimeNewsPage;

public class SkipLoginPageSteps {

    AskLoginPage askLoginPage = new AskLoginPage();

    FullAdvertisingPage fullAdvertisingPage = new FullAdvertisingPage();

    RealTimeNewsPage realTimeNewsPage =  new RealTimeNewsPage();

    @Given("^I am on AskLoginPage$")
    public void iAmOnAskLoginPage() throws Throwable {
        askLoginPage.verifyIndexShown();
    }

    @When("^I click on \"([^\"]*)\"$")
    public void iClickOn(String arg0) throws Throwable {
        askLoginPage.clickOn(arg0);
    }

    @Then("^FullAdvertisingPage is shown$")
    public void fulladvertisingpageIsShown() throws Throwable {
        fullAdvertisingPage.isShown();
    }

    @And("^I close advertising$")
    public void iCloseAdvertising() throws Throwable {
        fullAdvertisingPage.clickEtAdCoverHeader();
    }

    @Then("^I am on RealTimeNewsPage$")
    public void iAmOnRealTimeNewsPage() throws Throwable {
        realTimeNewsPage.verifyShown();
    }


}
