package pageObjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.DriverFactory;

import static org.junit.Assert.assertTrue;

public class AskLoginPage extends DriverFactory {

    private static Logger Log = LogManager.getLogger(AskLoginPage.class.getName());

    public AskLoginPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "logo")
    public WebElement logo;

    @AndroidFindBy(id = "register_btn")
    public WebElement registerButton;

    @AndroidFindBy(id = "login_btn")
    public WebElement loginButton;

    @AndroidFindBy(id = "skip_btn")
    public WebElement skipButton;

    public void verifyIndexShown() {
        waitVar.until(ExpectedConditions.visibilityOf(skipButton));
        assertTrue(logo.isDisplayed());
        assertTrue(registerButton.isDisplayed());
        assertTrue(loginButton.isDisplayed());
        assertTrue(skipButton.isDisplayed());
    }
    public void clickOn(String buttonName) throws InterruptedException {

        switch (buttonName) {
            case "registerButton":
                waitVar.until(ExpectedConditions.visibilityOf(registerButton));
                registerButton.click();
                break;
            case "loginButton":
                waitVar.until(ExpectedConditions.visibilityOf(loginButton));
                loginButton.click();
                break;
            case "skipButton":
                waitVar.until(ExpectedConditions.visibilityOf(skipButton));
                skipButton.click();
                break;
            default:
        }
    }
}
