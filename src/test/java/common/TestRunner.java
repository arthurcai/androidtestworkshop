package common;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.SnippetType;
import org.junit.runners.MethodSorters;


import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "steps",
        format = {"junit:target/JUnitReport.xml", "json:target/JSONReport.json"},
//        tags = {"@android"},
        snippets = SnippetType.CAMELCASE
)

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRunner {
//    private static AppiumDriverLocalService service;
    private static Logger Log = LogManager.getLogger(TestRunner.class.getName());
    /*
    @BeforeClass
    public static void startAppium() {

        String osName = System.getProperty("os.name").toLowerCase();
        
        String nodePath = null;
        String appiumPath = null;

        if (osName.contains("mac")) {
            // mac path
            nodePath = "/Users/cai/.nvm/versions/node/v7.0.0/bin/node";
            appiumPath = "/Users/cai/.nvm/versions/node/v7.0.0/lib/node_modules/appium/build/lib/main.js";
        } else if (osName.contains("linux")) {
            // linux path
           // nodePath = System.getenv("HOME") + "/.linuxbrew/bin/node";
            //appiumPath = System.getenv("HOME") + "/.linuxbrew/lib/node_modules/appium/build/lib/main.js";
            nodePath =  "/opt/node/bin/node";
            appiumPath = "/opt/node/lib/node_modules/appium/build/lib/main.js";
        } else if (osName.contains("windows 10")){
            nodePath = "C:\\Program Files\\nodejs\\node.exe";
            appiumPath = "C:\\appium\\build\\lib/main.js";
        }
        service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                                                        .usingDriverExecutable(new File(nodePath))
                                                        .usingPort(4723)
                                                        .withAppiumJS(new File(appiumPath)));
        service.start();
        System.out.println("=================="+service.getUrl());

        Log.info("===============================================================");
        Log.info("====             Android UI Tests Start !!!                ====");
        Log.info("===============================================================");
    }
    
    @AfterClass
    public static void stopAppium() {
        Log.info("================= Android UI Tests Stop =======================");
        service.stop();
    }
*/
}
