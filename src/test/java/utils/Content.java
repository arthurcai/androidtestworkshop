package utils;

/**
 * Created by user on 2017/1/10.
 */
public class Content {

    public static class titleName {
        public static final String IMMEDIATENEWS = "測試 / 新聞列表單列";
        public static final String IMAGE = "測試 / 圖集列表";
        public static final String MEDIA = "測試 / 影音單列";
        public static final String LIVE = "測試 / 直播";
        public static final String CHANNEL_SUBSCRPTION = "頻道訂閱";
    }

}
