package pageObjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.DriverFactory;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ToolBar extends DriverFactory {

    private static Logger Log = LogManager.getLogger(ToolBar.class.getName());

    public ToolBar() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(id = "bottom_navigation_container")
    private List<WebElement> bottomNavigationContainer;

    public void verifyShown() {
        assertNotNull(bottomNavigationContainer);
        assertTrue(bottomNavigationContainer.size()==5);

    }

    public void clickToolBarList(Integer index){
        if(index!=null){
            bottomNavigationContainer.get(index).click();
        }
    }

}
