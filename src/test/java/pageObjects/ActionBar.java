package pageObjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.DriverFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by user on 2017/1/23.
 */
public class ActionBar extends DriverFactory {

    private static Logger Log = LogManager.getLogger(RealTimeNewsPage.class.getName());

    @AndroidFindBy(className = "android.widget.ImageButton")
    public WebElement menu;

    @AndroidFindBy(id = "center_title")
    public WebElement centerTitle;

    public ActionBar() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void verifyShown(String titleName) {
        waitVar.until(ExpectedConditions.visibilityOf(centerTitle));
        assertTrue(menu.isDisplayed());
        assertTrue(centerTitle.isDisplayed());
        if(titleName!=null){
            assertTrue(centerTitle.getText().equals(titleName));
        }
    }

    public void clickMenu(){
        menu.click();
    }
}
