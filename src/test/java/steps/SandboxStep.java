package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.NewsContentPage;
import pageObjects.RealTimeNewsPage;

public class SandboxStep {
    RealTimeNewsPage realTimeNewsPage =  new RealTimeNewsPage();
    NewsContentPage newsContentPage = new NewsContentPage();
    @When("^I click on first news$")
    public void iClickOnFirstNews() throws Throwable {
        realTimeNewsPage.clickNews(0);
    }

    @Then("^I am on first news$")
    public void iAmOnFirstNews() throws Throwable {
        newsContentPage.verifyShown();
    }


    @When("^I click on Back$")
    public void iClickOnBack() throws Throwable {
        newsContentPage.clickBack();
    }
}
