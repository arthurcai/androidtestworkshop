package pageObjects;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.DriverFactory;

import static org.junit.Assert.assertTrue;


public class NewsContentPage extends DriverFactory {

    ToolBar toolBar = new ToolBar();

    ActionBar actionBar = new ActionBar();

    private static Logger Log = LogManager.getLogger(NewsContentPage.class.getName());

    public NewsContentPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "et_view_pager")
    private WebElement etWv;

    @AndroidFindBy(className = "android.webkit.WebView")
    private WebElement webView;

    @AndroidFindBy(className = "android.widget.ImageButton")
    private WebElement backBtn;


    public void verifyShown() {
        waitVar.until(ExpectedConditions.visibilityOf(etWv));
//        toolBar.verifyShown();
//        actionBar.verifyShown();
        assertTrue(webView.isDisplayed());
//        assertTrue(etWv.isDisplayed());
    }

    public void clickBack(){
        backBtn.click();
    }
}
