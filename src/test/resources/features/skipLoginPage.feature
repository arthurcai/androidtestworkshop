Feature: As a user, I could skip login steps on askLoginPage
  Scenario: when askLoginPage is displayed,
            after I click skipButton and close advertising,
            I should see the news list
    Given I am on AskLoginPage
    When I click on "skipButton"
    Then FullAdvertisingPage is shown
    And  I close advertising
    Then I am on RealTimeNewsPage

